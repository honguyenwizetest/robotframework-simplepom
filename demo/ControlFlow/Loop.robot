*** Settings ***
Documentation    Suite description
Library  String

*** Test Cases ***
Loop in range
    :FOR  ${index}  IN RANGE  0  3
    \   log  the current index: ${index}
    \   ${random_string} =  generate random string
    \   log  ${random_string}

Loop in range with upper
    :FOR  ${index}  IN RANGE  3
    \   log  the current index: ${index}
    \   ${random_string} =  generate random string
    \   log  ${random_string}

Loop in range with step
    :FOR  ${index}  IN RANGE  0  10  2
    \   log  the current index: ${index}
    \   ${random_string} =  generate random string
    \   log  ${random_string}

Loop in list
    @{LIST} =  create list  Item 1  Item 2  Item 3

    :FOR  ${item}  IN  @{LIST}
    \   log  ${item}

Loop in list with exit loop
    @{LIST} =  create list  Item 1  Item 2  Item 3

    :FOR  ${item}  IN  @{LIST}
    \   log  before if: ${item}
    \   run keyword if  "${item}" == "Item 3"  exit for loop
    \   log  after if: ${item}