*** Settings ***
Documentation    Suite description

*** Variables ***
${MY_VAR} =  100

*** Test Cases ***
Test If
    [Tags]    DEBUG
    run keyword if  ${MY_VAR} > 50  keyword 1

Test If Else
    [Tags]    DEBUG
    run keyword if  ${MY_VAR} > 200  keyword 1
    ...     ELSE  keyword 2

Test If Else If
    [Tags]    DEBUG
    run keyword if  ${MY_VAR} > 200  keyword 1
    ...     ELSE IF  ${MY_VAR} >150  keyword 2
    ...     ELSE  keyword 3

*** Keywords ***
Keyword 1
    log  this is keyword 1

Keyword 2
    log  this is keyword 2

Keyword 3
    log  this is keyword 3