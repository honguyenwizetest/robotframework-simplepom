*** Settings ***
Resource  ../MyResources/PO/LandingPage.robot
Resource  ../MyResources/PO/FilterComponent.robot
Resource  ../MyResources/PO/SearchResults.robot
Resource  ../MyResources/PO/ProductDetails.robot
Resource  ../MyResources/PO/ShoppingCart.robot
Resource  ../MyResources/PO/SignIn.robot

*** Keywords ***
user loads the home page
    LandingPage.user opens the home page
    LandingPage.user sees the home page loaded

user enters search condition into the product filter
    FilterComponent.user enters search term into the filter
    FilterComponent.user submit search term

user sees list of the results
    SearchResults.user sees the results displayed

user selects a product from the search result
    SearchResults.user clicks on the product link

user sees the corrected product loaded
    ProductDetails.user sees the correct product title

user adds the product to the cart
    ProductDetails.user clicks on Add to Cart

user sees the product added to cart
    ShoppingCart.user sees the product added to the cart

user loads the login page
    SignIn.user opens the login page
    SignIn.user sees the login page loaded

user logins with the default credential
    SignIn.user fills default "Email" field
    SignIn.user click Continue
    SignIn.user fills default "Password" field
    SignIn.user clicks "Sign In" button

user logins with credentials
    [Arguments]  ${login_email}  ${login_password}
    SignIn.user fills "Email" field  ${login_email}
    SignIn.user click Continue
    SignIn.user fills "Password" field  ${login_password}
    SignIn.user clicks "Sign In" button

user logins successfully
    LandingPage.user sees username on the home page

user logins failed
    SignIn.user sees invalid credentials