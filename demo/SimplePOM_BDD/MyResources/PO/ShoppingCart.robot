*** Variables ***
${CART_ADDED_TITLE} =  Added to Cart

*** Keywords ***
user sees the product added to the cart
    page should contain  ${CART_ADDED_TITLE}