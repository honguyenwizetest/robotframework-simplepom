*** Variables ***
${SINGIN_PAGE_TITLE} =  Sign-In
${HOMEPAGE_TITLE} =  Your Amazon.com
${SIGNIN_EMAIL_TEXTBOX} =  id:ap_email
${SIGNIN_PASSWORD_TEXTBOX} =  id:ap_password
${SIGNIN_CONTINUE_BUTTON} =  id:continue
${SIGNIN_BUTTON} =  id:signInSubmit

*** Keywords ***
user opens the login page
    go to  ${LOGIN_URL}

user sees the login page loaded
    Wait Until Page Contains  ${SINGIN_PAGE_TITLE}

user fills default "Email" field
    input text  ${SIGNIN_EMAIL_TEXTBOX}  ${LOGIN_EMAIL}

user fills default "Password" field
    input text  ${SIGNIN_PASSWORD_TEXTBOX}  ${LOGIN_EMAIL}

user click Continue
    click button  ${SIGNIN_CONTINUE_BUTTON}

user fills "Email" field
    [Arguments]  ${login_email}
    input text  ${SIGNIN_EMAIL_TEXTBOX}  ${login_email}

user fills "Password" field
    [Arguments]  ${login_password}
    input text  ${SIGNIN_PASSWORD_TEXTBOX}  ${login_email}

user clicks "Sign In" button
    click button  ${SIGNIN_BUTTON}

user sees invalid credentials
    log  Invalid username or password. Please try again!