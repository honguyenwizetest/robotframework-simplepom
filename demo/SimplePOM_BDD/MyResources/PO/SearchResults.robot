*** Variables ***
${SEARCH_RESULTS_PRODUCT_LINK} =  xpath:(//a[contains(text(),'${SEARCH_TERM}')])[1]

*** Keywords ***
user sees the results displayed
    wait until page contains  results for "${SEARCH_TERM}"

user clicks on the product link
    [Documentation]  Click on the first product in the search result
    log to console    ${SEARCH_TERM}
    Wait Until Page Contains Element   ${SEARCH_RESULTS_PRODUCT_LINK}
    sleep    5s
    Click Link  ${SEARCH_RESULTS_PRODUCT_LINK}