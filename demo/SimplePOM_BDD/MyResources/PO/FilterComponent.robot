*** Variables ***
${FILTER_SEARCH_TEXTBOX} =  id:twotabsearchtextbox
${FILTER_SEARCH_BUTTON} =  css:[type='submit'].nav-input

*** Keywords ***
user enters search term into the filter
    input text  ${FILTER_SEARCH_TEXTBOX}  ${SEARCH_TERM}

user submit search term
    click button  ${FILTER_SEARCH_BUTTON}

