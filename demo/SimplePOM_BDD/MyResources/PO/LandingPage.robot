*** Variables ***
${LANDING_PAGE_HEADER} =  Amazon.com

*** Keywords ***
user opens the home page
    go to  ${LANDING_URL}

user sees the home page loaded
    Wait Until Page Contains  ${LANDING_PAGE_HEADER}

user sees username on the home page
    log  username showing on the home page