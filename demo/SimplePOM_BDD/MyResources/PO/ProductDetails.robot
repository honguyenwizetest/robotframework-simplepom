*** Settings ***
Documentation    Suite description

*** Variables ***
${PRODUCT_DETAILS_ADD_TO_CART_BUTTON} =  id:add-to-cart-button

*** Keywords ***
user sees the correct product title
    sleep    3s
    page should contain  ${SEARCH_TERM}


user clicks on Add to Cart
    click button  ${PRODUCT_DETAILS_ADD_TO_CART_BUTTON}