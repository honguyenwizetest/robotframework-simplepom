*** Keywords ***
Begin web test
    open browser  about:blank  ${BROWSER}
    maximize browser window
    log  set up: begin test

End web test
    close browser
    log  tear down: end test

Insert test data
    log  insert test data

Cleanup test data
    log  cleanup test data

