*** Settings ***
Documentation    Suite description

Resource  ../MyResources/Imports.robot
Resource  ../MyResources/AmazonApp.robot

Suite Setup  Insert test data
Test Setup  Begin web test
Test Teardown  End web test
Suite Teardown  Cleanup test data

*** Test Cases ***
User can search for products
    [Documentation]  this is to test the search feature
    [Tags]  DEBUG
    When user loads the home page
    And user enters search condition into the product filter
    Then user sees list of the results


User can see the product details
    [Documentation]  this is to test the product details
    [Tags]  DEBUG
    When user loads the home page
    And user enters search condition into the product filter
    And user selects a product from the search result
    Then user sees the corrected product loaded

User can add a product to cart
    [Documentation]  this is to test adding to cart
    [Tags]  DEBUG
    When user loads the home page
    And user enters search condition into the product filter
    And user selects a product from the search result
    And user adds the product to the cart
    Then user sees the product added to cart
