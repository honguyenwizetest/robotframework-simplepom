*** Settings ***
Resource  ../MyResources/AmazonApp.robot
Resource  ../MyResources/Imports.robot

Test Setup  Begin web test
Test Teardown  End web test

*** Variables ***
${LOGIN_INVALID_EMAIL} =  wrongEmail@gmail.com
${LOGIN_INVALID_PASSWORD} =  wrongPassword

*** Test Cases ***
User can login successfully with valid Credentials
    [Tags]  DEBUG
   Given user loads the login page
   When user logins with the default credential
   Then user logins successfully

User can login unsuccessfully with invalid Credentials
    [Tags]  DEBUG
   Given user loads the login page
   When user logins with credentials  ${LOGIN_INVALID_EMAIL}  ${LOGIN_INVALID_PASSWORD}
   Then user logins failed