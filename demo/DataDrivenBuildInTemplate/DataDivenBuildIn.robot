*** Settings ***
Documentation    Suite description

# robot -d Results/ DataDrivenBuildInTemplate/DataDivenBuildIn.robot
*** Variables ***

&{UNREGISTERED_USER}  Email=admin@robotframeworktutorial.com  Password=TestPassword!  ExpectedErrorMessage=You haven't signed up yet. Try a different email address or
&{INVALID_PASSWORD_USER}  Email=bryan@bryanlamb.com  Password=TestPassword!  ExpectedErrorMessage=Please double check your password. It should be 6 or more characters with no spaces. If you don't remember it, you can
&{BLANK_CREDENTIALS_USER}  Email=#BLANK  Password=#BLANK  ExpectedErrorMessage=Please enter your email address and password.

*** Test Cases ***
Test Template
    [Template]  my test template
    one  two  three
    four  five  six
    seven  eight  nine

Invalid login scenarios should display correct error messages
    [Template]  Test Multiple Login Scenarios
    ${UNREGISTERED_USER}
    ${INVALID_PASSWORD_USER}
    ${BLANK_CREDENTIALS_USER}

*** Keywords ***
My Test Template
    [Arguments]  ${value1}  ${value2}  ${value3}
    log  first value: ${value1}
    log  second value: ${value2}
    log  third value: ${value3}

Test Multiple Login Scenarios
    [Arguments]  ${Credentials}
    log  Navigate to Sign In Page
    log  Attempt Login with ${Credentials.Email} ${Credentials.Password}
    log  Verify Login Page Error Message ${Credentials.ExpectedErrorMessage}