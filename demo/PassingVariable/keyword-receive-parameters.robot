*** Keywords ***
test keyword with scalar variables
    [Arguments]  ${url}  ${broswer}
    log  ${url}
    log  ${broswer}

test keyword with list variables
    [Arguments]  @{url_and_browser}
    log  @{url_and_browser}[0]
    log  @{url_and_browser}[1]