*** Settings ***
Resource  keyword-receive-parameters.robot

*** Test Cases ***
Test passing scalar variables
    [Tags]    smoke
    test keyword with scalar variables  www.google.com  firefox

Test passing variables list variables
    [Tags]    smoke
    @{url_browser} =  set variable  www.google.com  firefox
    test keyword with list variables  @{url_browser}
