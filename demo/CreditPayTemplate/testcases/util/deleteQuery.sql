create temporary table IF NOT EXISTS makro_id_del select makro_profile_id from acp_user where thai_id in (select thai_id from thai_id_del);
create temporary table IF NOT EXISTS user_id_del select id from acp_user where thai_id in (select thai_id from thai_id_del);
create temporary table IF NOT EXISTS ta_id_del select id from acp_ta_customer where user_id in (select id from user_id_del);
create temporary table IF NOT EXISTS loan_id_del select id from acp_loan where user_id in (select id from user_id_del);
create temporary table IF NOT EXISTS payment_id_del select id from acp_payment where user_id in (select id from user_id_del);

SET FOREIGN_KEY_CHECKS = 0;

delete from acp_repayment  where user_id in (select id from user_id_del);
delete from acp_loan where id in (select id from loan_id_del);

delete from acp_balance  where user_id in (select id from user_id_del);
delete from acp_credit_line  where user_id in (select id from user_id_del);

delete from acp_ta_customer  where user_id in (select id from user_id_del);
delete from acp_otp_tracking  where user_id in (select id from user_id_del);
delete from acp_topup  where payment_id in (select id from payment_id_del);
delete from acp_payment  where user_id in (select id from user_id_del);
delete from acp_user_token where user_id in (select id from user_id_del);
delete from acp_user_consent where user_id in (select id from user_id_del);
delete from acp_user_registration where user_id in (select id from acp_user where thai_id in (select thai_id from thai_id_del));
delete from acp_user where thai_id in (select thai_id from thai_id_del);
delete from acp_makro_profile where id in (select makro_profile_id from makro_id_del);

drop table if exists thai_id_del;
drop table if exists makro_id_del;
drop table if exists user_id_del;
drop table if exists ta_id_del;
drop table if exists loan_id_del;
drop table if exists payment_id_del;

SET FOREIGN_KEY_CHECKS = 1;