*** Settings ***
Documentation    Suite description

Resource  ../../keywords/page/seller_page_keywords.robot

Suite Setup  run keywords
...  Delete users info from 'dev/seller_data.yaml' with name 'payment_page'
...  open blank page
Suite Teardown  close browser
Test Setup  go to home page

*** Test Cases ***
Test case for searching products
    When user goes to the seller page
    Then user see the seller page header 'Amazon Best Sellers'

Test case for getting data
    [Tags]  smoke
    Log  ${seller_page.testcase_01.thai_id}
    Log  ${seller_page.testcase_01.makro_id}

