*** Settings ***
Documentation    Suite description

Resource  ../../keywords/page/release_page_keywords.robot

Suite Setup  run keywords
...  Delete users info from 'dev/release_data.yaml' with name 'release_page'
...  open blank page
Suite Teardown  close browser
Test Setup  go to home page

*** Test Cases ***
Test case for searching products
    When user goes to the release page
    Then user see the release page header 'Amazon Hot New Releases'

Test case for getting data
    [Tags]  smoke
    Log  ${release_page.testcase_01.thai_id}
    Log  ${release_page.testcase_01.makro_id}
