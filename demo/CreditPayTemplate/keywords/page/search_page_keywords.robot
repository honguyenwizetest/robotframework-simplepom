*** Settings ***
Resource  ../common/common_keywords.robot

*** Variables ***
${search_input} =  id:twotabsearchtextbox
${search_button} =  xpath://input[@type='submit']
${search_term} =  1234

*** Keywords ***
user enters search products
    wait until element is visible       ${search_input}       20 seconds
    input text  ${search_input}   ${search_term}
    Click Element   ${search_button}

user see the search term displayed
    page should contain  ${search_term}