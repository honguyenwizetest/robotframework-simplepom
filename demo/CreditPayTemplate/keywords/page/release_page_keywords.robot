*** Settings ***
Resource  ../common/common_keywords.robot

*** Keywords ***
user goes to the release page
    log  go to release page
    log  ${web_url}${frontend.new_release_path}
    go to  ${web_url}${frontend.new_release_path}

user see the release page header '${page_header}'
    page should contain  ${page_header}