*** Settings ***
Resource  ../common/common_keywords.robot

*** Keywords ***
user goes to the seller page
    log  go to config page
    log  ${web_url}${frontend.best_seller_path}
    go to  ${web_url}${frontend.best_seller_path}

user see the seller page header '${page_header}'
    page should contain  ${page_header}