*** Settings ***
Resource  ../../resources/imports.robot

*** Variables ***
${deleteQuery} =  ../../testcases/util/deleteQuery.sql

*** Keywords ***
open blank page
    open browser  about:blank  chrome

go to home page
    log  go to config page
    log  ${web_url}
    go to  ${web_url}

Delete users info from '${dictionary_path}' with name '${name}'
    import variables  ${CURDIR}/../../resources/testdata/${dictionary_path}
    log many  connect db with  ${DBNAME}    ${DBUSER}    ${DBPASS}    ${DBHOST}    ${DBPORT}
    log  insert user on db
    log  execute SQL script ${deleteQuery}