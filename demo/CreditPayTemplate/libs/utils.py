from Crypto.Cipher import AES
import hashlib
import hmac
import urllib3
import base64
import jwt
import json

"""
Disable the messsage:
InsecureRequestWarning: Unverified HTTPS request is being made. Adding certificate verification is strongly advised.
"""

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

def encrypt_signature(message):
  dig = hmac.new('789', msg=message, digestmod=hashlib.sha256).digest()
  return base64.b64encode(dig).decode()

def get_sha256_hash(thaiId):
    thaiId = str(thaiId)
    return hashlib.sha256(thaiId.encode()).hexdigest()

# Encryption/Decryption implemented according to backend (AES256 algorithm)
def decrypt_aes(cryptedStr, key, iv):
    BS = 16
    unpad = lambda s : s[0:-ord(s[-1])]
    generator = AES.new(key, AES.MODE_CBC, iv)
    cryptedStr = base64.b64decode(cryptedStr)
    recovery = generator.decrypt(cryptedStr)
    return unpad(recovery)

def encrypt_aes(cryptedStr, key, iv):
    BS = 16
    pad = lambda s: s + (BS - len(s) % BS) * chr(BS - len(s) % BS) 
    cryptedStr = pad(cryptedStr)
    generator = AES.new(key, AES.MODE_CBC, iv)
    cryptedStr = generator.encrypt(cryptedStr)
    recovery = base64.b64encode(cryptedStr)
    return recovery

def get_thai_id_checksum(thaiIdChars):
    sum = 0
    index = 0
    for number in thaiIdChars:
        sum = sum + int(number)*(13-index)
        index = index + 1
    return (11 - sum % 11) % 10

def generate_base64_encoded(secret):
    b64 = base64.b64encode(bytes(secret))
    b64 = str(b64);
    b64 = b64.split("'")
    return b64[0]

def generate_sns_jwt(payment_code):
    sns_json_string = """
    {
      "tmnId": "tmn.10000114539",
      "mobileNumber": "0909710052",
      "sellerName": "MAKRO",
      "merchant": {
        "merchantPhone": "66-811111111",
        "merchantId": "010000000002608452075",
        "merchantEmail": "aries@test.com"
      },
      "eventType": "PAYMENT",
      "ref1": "0909710052",
      "transactionDate": "2018-09-13T14:27:46+0700",
      "thaiIdHash": "809990b50de8e42ec3469c39b787a5c6d696013c91a8ba6c261bc71240393933",
      "customerNumber": "0909710052",
      "sourceOfFund": "ew",
      "merchantDisplayName": "Apple",
      "distinctId": "5e4b4e076e5d532b3d363a30b2a7d2b30e94a49d",
      "sellerId": "300000000000000036763",
      "productCode": "51051000100000000017",
      "requestId": "18091314274687217KFA",
      "mobileNoHash": "d3826a3de0eec0cdbc5c7850b0680e45352da3511dc4c138a87c7bff59488154",
      "productDescription": "title test by pp (Perseus team)2",
      "moneyAmount": 123456
    }
    """
    sns_json = json.loads(sns_json_string)
    sns_json["kycVerifyStatus"]= False
    sns_json["paymentCode"]= payment_code
    return str(jwt.encode(payload = sns_json, key="secret", algorithm="HS256"));

def generate_sns_jwt_with_inputted_payment_code_and_amount(payment_code, amount):
    sns_json_string = """
    {
      "tmnId": "tmn.10000114539",
      "mobileNumber": "0909710052",
      "sellerName": "makro",
      "merchant": {
        "merchantPhone": "66-811111111",
        "merchantId": "010000000002608452075",
        "merchantEmail": "aries@test.com"
      },
      "eventType": "PAYMENT",
      "ref1": "0909710052",
      "transactionDate": "2018-09-13T14:27:46+0700",
      "thaiIdHash": "809990b50de8e42ec3469c39b787a5c6d696013c91a8ba6c261bc71240393933",
      "customerNumber": "0909710052",
      "sourceOfFund": "ew",
      "merchantDisplayName": "Apple",
      "distinctId": "5e4b4e076e5d532b3d363a30b2a7d2b30e94a49d",
      "sellerId": "300000000000000036763",
      "productCode": "51051000100000000017",
      "requestId": "18091314274687217KFA",
      "mobileNoHash": "d3826a3de0eec0cdbc5c7850b0680e45352da3511dc4c138a87c7bff59488154",
      "productDescription": "title test by pp (Perseus team)2"
    }
    """
    sns_json = json.loads(sns_json_string)
    sns_json["kycVerifyStatus"]= False
    sns_json["moneyAmount"] = amount
    sns_json["paymentCode"]= payment_code
    return str(jwt.encode(payload = sns_json, key="secret", algorithm="HS256"));

def generate_sns_jwt_with_given_payment_code_amount_and_payment_chanel(payment_code, amount, payment_chanel):
    sns_json_string = """
    {
      "tmnId": "tmn.10000114539",
      "mobileNumber": "0909710052",
      "merchant": {
        "merchantPhone": "66-811111111",
        "merchantId": "010000000002608452075",
        "merchantEmail": "aries@test.com"
      },
      "eventType": "PAYMENT",
      "ref1": "0909710052",
      "transactionDate": "2018-09-13T14:27:46+0700",
      "thaiIdHash": "809990b50de8e42ec3469c39b787a5c6d696013c91a8ba6c261bc71240393933",
      "customerNumber": "0909710052",
      "sourceOfFund": "ew",
      "merchantDisplayName": "Apple",
      "distinctId": "5e4b4e076e5d532b3d363a30b2a7d2b30e94a49d",
      "sellerId": "300000000000000036763",
      "productCode": "51051000100000000017",
      "requestId": "18091314274687217KFA",
      "mobileNoHash": "d3826a3de0eec0cdbc5c7850b0680e45352da3511dc4c138a87c7bff59488154",
      "productDescription": "title test by pp (Perseus team)2"
    }
    """
    sns_json = json.loads(sns_json_string)
    sns_json["kycVerifyStatus"]= False

    sns_json["moneyAmount"] = amount
    sns_json["sellerName"] = payment_chanel
    sns_json["paymentCode"]= payment_code
    return str(jwt.encode(payload = sns_json, key="secret", algorithm="HS256"));

def get_sns_request_body(message_payload):
    body = """
    {"Type" : "Notification",
    "MessageId" : "22b80b92-fdea-4c2c-8f9d-bdfb0c7bf324",
    "TopicArn" : "arn:aws:sns:us-west-2:123456789012:MyTopic",
    "Subject" : "My First Message",
    "Timestamp" : "2012-05-02T00:54:06.655Z",
    "SigningCertURL" : "https://sns.us-west-2.amazonaws.com/SimpleNotificationService-f3ecfb7224c7233fe7bb5f59f96de52f.pem",
    "SignatureVersion" : "1",
    "Signature" : "EXAMPLEw6JRNwm1LFQL4ICB0bnXrdB8ClRMTQFGBqwLpGbM78tJ4etTwC5zU7O3tS6tGpey3ejedNdOJ+1fkIp9F2/LmNVKb5aFlYq+9rk9ZiPph5YlLmWsDcyC5T+Sy9/umic5S0UQc2PEtgdpVBahwNOdMW4JPwk0kAJJztnc=",
    "UnsubscribeURL" : "https://sns.us-west-2.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-west-2:123456789012:MyTopic:c9135db0-26c4-47ec-8998-413945fb5a96",
    """
    body = body + "\"Message\"" + ":" + "\"" + message_payload + "\"}"
    return body


