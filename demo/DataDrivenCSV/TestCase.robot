*** Settings ***
Documentation    Suite description
Resource  DataManagement.robot

#robot -d Results/ DataDrivenCSV/TestCase.robot

*** Variables ***
${INVALID_CREDENTIALS_PATH_CSV} =  ./CSVDataDriven/Data/Users.csv

*** Test Cases ***
Should see error messages with invalid logins
    ${Users} =  DataManagement.get csv data  ${INVALID_CREDENTIALS_PATH_CSV}
    log  ${Users}
    Login with Many Invalid Credentials  ${Users}

*** Keywords ***
Login with Many Invalid Credentials
    [Arguments]  ${InvalidCredentials}
    :FOR  ${LoginScenario}  IN  @{InvalidCredentials}
    \  log many  ${LoginScenario[0]}  ${LoginScenario[1]}  ${LoginScenario[2]}