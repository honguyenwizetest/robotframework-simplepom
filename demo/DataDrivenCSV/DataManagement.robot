*** Settings ***
Library  ./Lib/Csv.py

*** Keywords ***
Get csv data
    [Arguments]  ${FilePath}
    ${Data} =  read csv file  ${FilePath}
    [Return]  ${Data}
