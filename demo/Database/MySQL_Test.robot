*** Settings ***
Documentation    Suite description
Library  DatabaseLibrary
Library  String

# run the following commands to install databaselibrary & pymssql
# pip install -U robotframework-databaselibrary
# brew unlink freetds; brew install homebrew/core/freetds091
# brew link --force freetds@0.91
# pip install pymysql

#robot -d demo/Database/Results/ demo/Database/MySQL_Test.robot

Test Setup  Connect Database
Test Teardown  Disconnect Database

*** Variables ***
${PREVIOUS_ROW_COUNT}
${INSERT_NAME} =  Default name1
${INSERT_ADDRESS} =  Default address1

*** Test Cases ***
Insert a record
    Save Current Row Count
    Insert Record
    Verify New Record Added
*** Keywords ***
Connect Database
    log  start connection
#    connect to database using custom params  pymysql  database='demodb', user='root', password='admin123', host='127.0.0.1', port=3306
    connect to database using custom params  pymysql  database='demodb', user='user', password='password', host='10.4.5.26', port=3306
Disconnect Database
    disconnect from database

Save Current Row Count
    ${current_row_count} =  row count  select * from demodb.userprofile;
    set suite variable  ${PREVIOUS_ROW_COUNT}  ${current_row_count}
    log  ${current_row_count}

Insert Record
    ${insert_command} =  set variable  INSERT INTO userprofile (name, address) VALUES ('${INSERT_NAME}', '${INSERT_ADDRESS} ')
    execute sql string  ${insert_command}

Verify New Record Added
    ${new_row_count} =  row count  select * from demodb.userprofile;
    log  ${new_row_count}
    log  ${PREVIOUS_ROW_COUNT +1}
    should be equal as numbers  ${new_row_count}  ${PREVIOUS_ROW_COUNT +1}