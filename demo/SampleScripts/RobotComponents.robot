*** Settings ***
Documentation    Suite description
Library  SeleniumLibrary

*** Variables ***
${urlToBrowser} =  http://www.amazon.com
${browserType} =  chrome
@{arrayVariable} =  val 1  val 2  val 3
*** Test Cases ***
Open Google page
    [DOCUMENTATION]  Check Amazon website - Google
    [Tags]    DEBUG
    open browser  http://www.google.com  ${browserType}
    sleep  3
    close browser

Open Amazon page
    [DOCUMENTATION]  Check Amazon website
    [Tags]    RELEASE
    open browser  http://www.amazon.com
    wait until page contains  Today's Deals
    Input Text  id:twotabsearchtextbox  Ferrari 458
    Click Button  css:input[type=submit][class=nav-input]
    page should contain  results for "Ferrari 458"
    Click Link  xpath://*[contains(text(),'Ferrari 458 Italia WALL DECAL')]
    Page Should Contain Link   Back to results
    close browser

Demo Open URL using variable
    [Tags]  VARIABLE
    open browser  ${urlToBrowser}  ${browserType}
    sleep  2
    close browser

Demo Set Variable into a test case
    [Tags]  VARIABLE

    ${browserType} =  set variable  firefox
    log  ${browserType}

    @{arrayVariable} =  set variable  val 0  val 01  val 02
    log  @{arrayVariable}[0]
    log  @{arrayVariable}[1]
    log  @{arrayVariable}[2]

Demo simple keywords
    [Tags]  ANYTHING
    do something
    do another thing

Demo passing parameters to a keyword into a test case
    [Tags]  PASSING
    pass parameters to open url  this_is_arg_1  this_is_arg_2
    pass multi-parameters to open url  @{arrayVariable}

*** Keywords ***
do something
    log  "Doing something"

do another thing
    log  "Doing another thing "

pass parameters to open url
    [Arguments]  ${val1}  ${val2}
    log  ${val1}
    log  ${val2}

pass multi-parameters to open url
    [Arguments]  @{multiVa}
    log  @{multiVa}[0]
    log  @{multiVa}[1]
    log  @{multiVa}[2]

