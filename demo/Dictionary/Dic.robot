*** Settings ***
Documentation    Suite description

*** Variables ***
${ENV} =  qa
${BROWSER} =  firefox
&{URL}  dev=https://www.google.com/  qa=https://www.amazon.com/
&{USER}  FirstName=Ho  LastName=Nguyen  Email=honguyen@gmail.com  Pass=honguyen123

*** Test Cases ***
Test dictionary
    log  ${URL.dev}
    log  ${URL.${ENV}}
    log many  ${USER.FirstName}  ${USER.LastName}  ${USER.Email}  ${USER.Pass}

Test to pass dictionary para
    get dictionary para  ${USER}

Test dictionary variable
    ${my_profile} =  create dictionary  FirstName=Doctor  LastName=Smith  Email=doctorsmith@gmail.com  Pass=doctorsmith123
    log many  ${my_profile.FirstName}  ${my_profile.LastName}  ${my_profile.Email}  ${my_profile.Pass}
*** Keywords ***
Get dictionary para
    [Arguments]  ${userInfor}
    log many  ${userInfor.FirstName}  ${userInfor.LastName}  ${userInfor.Email}  ${userInfor.Pass}