*** Settings ***
Documentation  this is a feature
Library  SeleniumLibrary

*** Variables ***
${MYVAL} =  this in variable1
@{MY_MUL_VAL} =  va1  val2  val3
*** Test Cases ***

Test case 1 - Global variable
    [Tags]  smoke
    log  ${MYVAL}
    log  @{MY_MUL_VAL}[0]
    log  @{MY_MUL_VAL}[1]
    log  @{MY_MUL_VAL}[2]

Test case 2 - Test case variable
    [Documentation]  basic test here
    [Tags]  regression  smoke
    ${myva_inter_1} =  set variable  this is inter-variable1
    log  ${myva_inter_1}

    @{myva_inter_2} =  Set Variable  internal variable_1  internal variable_2
    log  ${myva_inter_2}[0]
    log  ${myva_inter_2}[1]



