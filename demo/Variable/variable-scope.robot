*** Settings ***
Resource  variable-scope-keyword.robot

*** Variables ***
${MY_VARIABLE} =  variable from TESTCASE

*** Test Cases ***
Test variable from keyword override test case variable
    [Tags]    smoke
    my keyword to test variable

Test veriable from keyword only
   [Tags]  smoke2
   log  ${MY_VARIABLE_NEW}
