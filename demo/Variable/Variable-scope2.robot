*** Settings ***
Documentation    Suite description
Library  SeleniumLibrary

*** Variables ***
${GLOBAL} =  this is global variable
${GLOBAL_DATA}

*** Test Cases ***
Test case set and declare variables
    set global variable  ${GLOBAL}  this is global variable set inside a test case
    set global variable  ${GLOBAL_DATA}  this is global data set inside a test case
    set suite variable  ${suite_data}  this is suite data set in a test case
    set test variable  ${local_variable}  this is a local variale in a test case

Test case logging variable
    log to console  all variables will be shown below
#    log variables
#    comment  this is to log each Variable
    log  ${GLOBAL}
    log  ${GLOBAL_DATA}
    log  ${suite_data}
    comment  ${local_variable}

    set global variable  ${GLOBAL}  this is global variable NEW
    set global variable  ${GLOBAL_DATA}  this is global data NEW
    set suite variable  ${suite_data}  this is suite data NEW

Test case to ingnore failed cases
    run keyword and continue on failure  Wait until page contains  nothing
    log  ${GLOBAL}
    log  ${GLOBAL_DATA}
    log  ${suite_data}
    comment  ${local_variable}

Test case to repeat
    repeat keyword  5 times  log  this to repeat
    log  ${GLOBAL}
    log  ${GLOBAL_DATA}
    log  ${suite_data}
    comment  ${local_variable}
*** Keywords ***
Provided precondition
    Setup system under test