*** Settings ***
Documentation  this is to test the workflow from entering the search term to get the search results before opening the details page
Library  SeleniumLibrary

Test Setup  begin web test
Test Teardown  end web test
*** Variables ***
${URL} =  http://www.amazon.com
${BROWSER} =  chrome
${PAGE_HEADER} =  Your Amazon.com
${SEARCH_TERM} =  Ferrari 458
${SEARCH_TEXTBOX_LOCATOR} =  id:twotabsearchtextbox
${SEARCH_BUTTON_LOCATOR} =  xpath://*[@id="nav-search"]//input[@type='submit']
${PRODUCT_DETAIL_LINK_LOCATOR} =  xpath:(//*[contains(text(),'${SEARCH_TERM}')]/parent::a)[1]

*** Test Cases ***
Test case - Search products
    [Documentation]  Check Search
    [Tags]  smoke
    go to the home page
    enter search term
    check rearch results

Test case - check the product details
    [Documentation]  Check Search
    [Tags]  smoke
    go to the home page
    enter search term
    check rearch results
    go to a product details
    check the product details page loaded

*** Keywords ***
begin web test
    open browser  about:blank  ${BROWSER}
    maximize browser window
    log  set up: begin test

end web test
    close browser
    log  tear down: end test

go to the home page
    go to  ${URL}
    wait until page contains  ${PAGE_HEADER}
enter search term
    input text  ${SEARCH_TEXTBOX_LOCATOR}  ${SEARCH_TERM}
    click button  ${SEARCH_BUTTON_LOCATOR}

check rearch results
    wait until page contains  results for "${SEARCH_TERM}"

go to a product details
    click link  ${PRODUCT_DETAIL_LINK_LOCATOR}

check the product details page loaded
    wait until page contains   Back to results

clean up the test
    close browser