*** Settings ***
Documentation  this is to test the workflow from entering the search term to get the search results before opening the details page
Library  SeleniumLibrary

*** Variables ***
${URL} =  http://www.amazon.com
${BROWSER} =  chrome
${PAGE_HEADER} =  Your Amazon.com
${SEARCH_TERM} =  Ferrari 458
${SEARCH_TEXTBOX_LOCATOR} =  id:twotabsearchtextbox
${SEARCH_BUTTON_LOCATOR} =  xpath://*[@id="nav-search"]//input[@type='submit']
${PRODUCT_DETAIL_LINK_LOCATOR} =  xpath:(//*[contains(text(),'${SEARCH_TERM}')]/parent::a)[1]

*** Test Cases ***
Test case - Search products
    [Documentation]  Check Search
    [Tags]  smoke
    open the home page
    enter search term
    check rearch results
    go to a product details
    check the product details page loaded
    clean up the test

*** Keywords ***
open the home page
    open browser  ${URL}  ${BROWSER}
    wait until page contains  ${PAGE_HEADER}
enter search term
    input text  ${SEARCH_TEXTBOX_LOCATOR}  ${SEARCH_TERM}
    click button  ${SEARCH_BUTTON_LOCATOR}

check rearch results
    wait until page contains  results for "${SEARCH_TERM}"

go to a product details
    click link  ${PRODUCT_DETAIL_LINK_LOCATOR}

check the product details page loaded
    wait until page contains   Back to results

clean up the test
    close browser