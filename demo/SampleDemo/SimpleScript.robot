*** Settings ***
Documentation  this is to test the workflow from entering the search term to get the search results before opening the details page
Library  SeleniumLibrary

*** Test Cases ***
Test case - Open Page
    [Documentation]  basic test here
    [Tags]  regression  smoke
#    open the home page
    open browser  http://www.amazon.com  chrome
    wait until page contains  Your Amazon.com

#    enter search term
    input text  id:twotabsearchtextbox  Ferrari 458
    click button  xpath://*[@id="nav-search"]//input[@type='submit']

#    check rearch results
    wait until page contains  results for "Ferrari 458"

#go to a product details
    click link  xpath:(//*[contains(text(),'Ferrari 458')]/parent::a)[1]

#check the product details page loaded
    wait until page contains   Back to results

#clean up the test
    close browser