*** Settings ***
Documentation    Suite description
Library  RequestsLibrary
Library  Collections
Library  SeleniumLibrary

*** Test Cases ***
Test GET git username
    check Github username
#
Test GET display emoji
    display emoji

Test 2 GET requests
    call 2 GET requests

Test POST request
    [tags]  post2
    call POST request

Test POST request
    [tags]  put2
    call PUT request
*** Keywords ***
check Github username
    create session  git_hub_session  https://api.github.com

    ${response} =  get request  git_hub_session  users/robotframeworktutorial
    ${json} =  set variable  ${response.json()}

    should be equal as strings  ${response.status_code}  200
    should be equal as strings  ${json['name']}  Bryan Lamb
    dictionary should contain value  ${json}  Bryan Lamb

display emoji
    create session  git_hub_session  https://api.github.com

    ${res} =  get request  git_hub_session  emojis
    ${json_res} =  set variable  ${res.json()}

    should be equal as strings  ${res.status_code}  200

    log  ${json_res}
    log  ${json_res['accept']}

    ${accept_url} =  set variable  ${json_res['accept']}
#    open browser  ${accept_url}  chrome

call 2 GET requests
    Create Session  google_session  http://www.google.com
    Create Session  git_hub_session  https://api.github.com

    ${resp}=  Get Request  google_session  /
    should be equal as strings  ${resp.status_code}  200
    log  ${resp}

    ${resp}=  Get Request  git_hub_session  users/robotframeworktutorial
    should be equal as strings  ${resp.status_code}  200
    log  ${resp.json()}

call POST request
    create session  bin_session  http://httpbin.org

    ${data} =  create dictionary  fname=ho  lnam=nguyen  fullname=honguyen
    ${header} =  create dictionary  Content-Type=application/x-www-form-urlencoded

    ${res} =  post request  bin_session  /post  headers=${header}  data=${data}

    should be equal as strings  ${res.status_code}  200

    log  ${res.json()}
    dictionary should contain value  ${res.json()['form']}  honguyen

call PUT request
    create session  bin_session  http://httpbin.org

    ${data} =  create dictionary  fname=ho  lnam=nguyen  fullname=honguyen
    ${header} =  create dictionary  Content-Type=application/x-www-form-urlencoded

    ${res} =  put request  bin_session  /put  headers=${header}  data=${data}

    should be equal as strings  ${res.status_code}  200

    log  ${res.json()}
    dictionary should contain value  ${res.json()['form']}  honguyen