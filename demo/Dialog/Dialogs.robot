*** Settings ***
Documentation    Suite description
Library  Dialogs
Library  SeleniumLibrary

*** Variables ***
${BROWSER} =  chrome

*** Test Cases ***
Open URL by default BROWSER variable
    Open Browser  about:blank  ${BROWSER}
    Go To  https://www.amazon.com
    ${new_browser} =  get selection from user  Select the browser  firefox  ie  chrome
    set global variable  ${BROWSER}  ${new_browser}
    Close Browser

Open URL by changed BROWSER variable by dialog
    Open Browser  about:blank  ${BROWSER}
    Go To  https://www.amazon.com
    Close Browser